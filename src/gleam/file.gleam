import gleam/posix_errors.{PosixError}

//---------------------------------------------
//
// Working with files
//
//---------------------------------------------
// 
/// Returns Ok(String), where String  is a binary data object that contains the contents of Filename, or Error(PosixError) if an error occurs.
/// ## Typical error reasons:
///    Enoent
///        The file does not exist.
///    Eacces
///        Missing permission for reading the file, or for searching one of the parent directories.
///    Eisdir
///        The named file is a directory.
///    Enotdir
///        A component of the filename is not a directory. On some platforms, enoent is returned instead.
///    Enomem
///        There is not enough memory for the contents of the file.
pub fn read_to_bitstring(
  filename filename: String,
) -> Result(BitString, PosixError) {
  do_read_to_bitstring(filename)
}

external fn do_read_to_bitstring(
  filename: String,
) -> Result(BitString, PosixError) =
  "file" "read_file"

/// Returns Ok(Int), where Int is the number of bytes that was copied,
/// If the operation fails, Error(PosixError) is returned.
/// Typical error reasons are a combination of read and write errors.
pub fn copy(
  source source: String,
  destination destination: String,
) -> Result(Int, PosixError) {
  do_copy(source, destination)
}

external fn do_copy(
  source: String,
  destination: String,
) -> Result(Int, PosixError) =
  "file" "copy"

/// Tries to delete file Filename. Returns Ok(Nil)if successful.
/// ## Typical error reasons:
///    Enoent
///        The file does not exist.
///    Eacces
///        Missing permission for the file or one of its parents.
///    Eperm
///        The file is a directory and the user is not superuser.
///    Enotdir
///        A component of the filename is not a directory. On some platforms, enoent is returned instead.
///    Einval
///        Filename has an improper type, such as tuple.
pub external fn delete(filename: String) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "delete"

/// Writes the contents to file filename. The file is created if it does not exist.
/// If it exists, the previous contents are overwritten. Returns Ok(Nil) if successful,
/// otherwise Error(PosixError)
///
/// ## Typical error reasons:
///    Enoent
///        A component of the filename does not exist.
///    Enotdir
///        A component of the filename is not a directory.
///    Enoent
///        On some platforms, enoent is returned if a component of the filename is a directiory.
///    Enospc
///        No space is left on the device.
///    Eacces
///        Missing permission for writing the file or searching one of the parent directories.
///    Eisdir
///        The named file is a directory.
pub fn write_string(
  filename filename: String,
  contents contents: String,
) -> Result(Nil, PosixError) {
  do_write_string(filename, contents)
}

external fn do_write_string(
  filename: String,
  contents: String,
) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "write_string"

//---------------------------------------------
//
// Working with directories
//
//---------------------------------------------
/// Returns the current user's home directory
/// HOME needs to be set in the environment
pub external fn user_home() -> Result(String, Nil) =
  "gleam_file_bridge" "user_home"

/// Retutrns the current working directory
pub external fn cwd() -> Result(String, PosixError) =
  "gleam_file_bridge" "cwd"

/// Lists all files in a directory, except files with raw filenames.
/// Returns Ok(List(String)) if successful, otherwise Error(PosixError).
/// The names are not sorted.
/// 
/// ## Typical error reasons:
///    Eacces
///        Missing search or write permissions for Dir or one of its parent directories.
///    Enoent
///        The directory does not exist.
///    NoTranslation
///        Filename is a binary() with characters coded in ISO Latin-1 and the VM was started with parameter +fnue.
pub external fn list_directory(path: String) -> Result(List(String), PosixError) =
  "gleam_file_bridge" "list_directory"

/// Tries to delete directory Dir. The directory must be empty before it can be deleted. Returns ok if successful.
/// ## Typical error reasons:
///    Eacces
///        Missing search or write permissions for the parent directories of Dir.
///    Eexist
///        The directory is not empty.
///    Enoent
///        The directory does not exist.
///    Enotdir
///        A component of Dir is not a directory. On some platforms, enoent is returned instead.
///    Einval
///        Attempt to delete the current directory. On some platforms, eacces is returned instead.
pub external fn delete_empty_directory(path: String) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "delete_empty_directory"

/// Deletes the file or directory specified. If the path is a directory, its contents is first recursively deleted.
///
/// ## Typical error reasons:
///    same as delete_empty_directory
///
///    Eexsist
///        If some file or directory under File could not be deleted.
pub external fn delete_non_empty_directory(
  path: String,
) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "delete_non_empty_directory"

/// Tries to create directory Dir. Missing parent directories are not created. Returns ok if successful.
/// 
/// ## Typical error reasons:
///    Eacces
///        Missing search or write permissions for the parent directories of Dir.
///    Eexist
///        A file or directory named Dir exists already.
///    Enoent
///        A component of Dir does not exist.
///    Enospc
///        No space is left on the device.
///    Enotdir
///        A component of Dir is not a directory. On some platforms, enoent is returned instead.
pub external fn make_directory(path: String) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "make_directory"

/// Returns true if the given path exists.
/// It can be a regular file, directory, socket, symbolic link, named pipe,
/// or device file. Returns false for symbolic links pointing to non-existing targets.
pub external fn exists(path: String) -> Bool =
  "gleam_file_bridge" "exists"

/// Returns true if path refers to a directory, otherwise false.
pub external fn is_directory(path: String) -> Bool =
  "filelib" "is_dir"

/// Returns true if path refers to a (regular) file, otherwise false.
pub external fn is_file(path: String) -> Bool =
  "filelib" "is_file"

/// Returns the size of the specified file.
pub external fn size(path: String) -> Int =
  "filelib" "file_size"

/// Ensures that all parent directories for the specified path exist,
/// trying to create them if necessary.
pub external fn ensure_directory(path: String) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "ensure_directory"

/// Sets the current working directory of the file server to the specified path.
/// ## Typical error reasons are:
///    enoent
///        The directory does not exist.
///    enotdir
///        A component of Dir is not a directory. On some platforms, enoent is returned.
///    eacces
///        Missing permission for the directory or one of its parents.
///    badarg
///        Dir has an improper type, such as tuple.
///    no_translation
///        Dir is a binary() with characters coded in ISO-latin-1 and the VM is operating with unicode filename encoding.
pub external fn cd(path: String) -> Result(Nil, PosixError) =
  "gleam_file_bridge" "cd"
