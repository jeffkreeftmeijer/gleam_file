import gleam/should
import gleam/path.{Absolute}
import gleam/file
import gleam/result

pub fn basename_test() {
  path.basename("foo")
  |> should.equal("foo")

  path.basename("/usr/foo")
  |> should.equal("foo")

  path.basename("/")
  |> should.equal("")
}

pub fn extension_test() {
  path.extension("foo.gleam")
  |> should.equal(".gleam")
  path.extension("beam.src/kalle")
  |> should.equal("")
}

pub fn split_test() {
  path.split("foo/bar/baz")
  |> should.equal(["foo", "bar", "baz"])
  path.split("/foo/bar/baz")
  |> should.equal(["/", "foo", "bar", "baz"])
}

pub fn join_test() {
  path.join(["foo", "bar", "baz"])
  |> should.equal("foo/bar/baz")
  path.join(["/foo", "bar", "baz"])
  |> should.equal("/foo/bar/baz")
}

pub fn integration_test() {
  file.user_home()
  |> result.map(path.kind)
  |> should.equal(Ok(Absolute))
}
