# gleam_file

File, Directory, and Path handling in Gleam

## Documentation
https://hexdocs.pm/gleam_file

## Installation
Add `gleam_file` to your `rebar.config` dependencies:

```erlang
{deps, [
    gleam_file
]}.
```

## Quick start

```rust
import gleam/file

> file.read_to_bitstring("hi.txt")
Ok(<<"Hello from the contents of hi.txt">>)

```

## Contributing

This is very much a work in progress, feel free to contribute!

Things to do:
  - [ ] test everything
  - [ ] test on windows
  - [ ] write win32_x_tests
  - [ ] improve documentation
  - [ ] `file.read_to_string`
  - [ ] `path.expand`

Things to note:

Filenames in erlang are not necessarily utf8. Strings in gleam are utf8. You
must ensure filenames are utf8 before leaving erlang. You don't need to convert
going back into erlang.

